# Changelog


## v0.1.5

[compare changes](https://codeberg.org/Expo/svelte-bi/compare/v0.1.4...v0.1.5)

### 🚀 Enhancements

- New nexusfonts url (327daab)

### ❤️ Contributors

- Expo <expo@expo.moe>

## v0.1.4

[compare changes](https://codeberg.org/Expo/svelte-bi/compare/v0.1.3...v0.1.4)

### 🩹 Fixes

- Add vite plugin svelte as devdep (32caac2)
- Mark as not stale (da3e6cf)

### ❤️ Contributors

- Expo <expo@expo.moe>

## v0.1.3

[compare changes](https://codeberg.org/Expo/svelte-bi/compare/v0.1.2...v0.1.3)

### 🚀 Enhancements

- Upgrade deps (04d1401)

### ❤️ Contributors

- Expo <expo@expo.moe>

## v0.1.2

[compare changes](https://codeberg.org/Expo/svelte-bi/compare/v0.1.1...v0.1.2)

### 🚀 Enhancements

- README Badges (02debb7)

### 🩹 Fixes

- Specify entrypoint (e3f3656)

### ❤️ Contributors

- Expo <expo@expo.moe>

## v0.1.1

[compare changes](https://codeberg.org/Expo/svelte-bi/compare/v0.1.0...v0.1.1)

### 🩹 Fixes

- What is the changelog doin (bb2d9d0)
- Update Publish Branch (ff3a750)

### ❤️ Contributors

- Expo <expo@expo.moe>

## v0.1.0

[compare changes](https://codeberg.org/Expo/svelte-bi/compare/v0.0.5...v0.1.0)

### 🩹 Fixes

- It's actually v0.0.5, not v0.0.6 (aeb5f86)
- Push & push tags prior to publishing (0e0c245)
- Changelog should also include other icons (65dee32)
- Capitalize (edbf87a)
- Make bootstrap-icons non-required dep (56703c0)

### 🏡 Chore

- Change playground to use loadfontnexus (c12d283)

### ❤️ Contributors

- Expo <expo@expo.moe>

## v0.0.5

[compare changes](https://codeberg.org/Expo/svelte-bi/compare/0.0.4...v0.0.5)

### 🚀 Enhancements

- Add NexusFonts Loading (9b9cd5f)

### ❤️ Contributors

- Expo <expo@expo.moe>

